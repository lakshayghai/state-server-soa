###Steps to run the project using command Line
1. Checkout the code from bitbucket
   1. git clone git clone git@bitbucket.org:lakshayghai/state-server-soa.git
2. Go to the directory where you checked out the code
3. Run commands 
    1. mvn clean package
    2. java -jar target/state-server.jar --server.port=8090

###I have also staged the jar file in the downloads section. Saving steps 1,2,3 from above. Follow steps
1. Download the jar 
    1. https://bitbucket.org/lakshayghai/state-server-soa/downloads/state-server.jar
2. Go to the directory where you downloaded the jar
3. run command java -jar state-server.jar --server.port=8090

###Once the server is running you can test the webservice
1. Swagger UI
    1. http://localhost:8090/swagger-ui.html
2. From any rest client (eg Postman)
    1. ![Header](/img/Header.png)
    2. ![Body](/img/Body.png)
3. Curl command
    1. curl -d '{"longitude":-76.479212,"latitude":38.965453}' -H 'Content-Type: application/json'  http://localhost:8090/latitude-longitude/state
    
    
#### Documentation of the Webservice is there on the Swagger UI
* http://localhost:8090/swagger-ui.html
* The Webservice accepts JSON data as input.

    