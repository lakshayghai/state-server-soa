package com.vistar.interviewassignment.entity;

import java.util.List;
import java.util.Objects;

public class StateJson {

    private String state;
    private List<List> border;

    public StateJson() {
    }

    public StateJson(String state) {
        this.state = state;
    }

    public StateJson(String state, List<List> border) {
        this.state = state;
        this.border = border;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public List<List> getBorder() {
        return border;
    }

    public void setBorder(List<List> border) {
        this.border = border;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StateJson state = (StateJson) o;
        return Objects.equals(this.state, state.state) &&
                Objects.equals(border, state.border);
    }

    @Override
    public int hashCode() {
        return Objects.hash(state, border);
    }
}
