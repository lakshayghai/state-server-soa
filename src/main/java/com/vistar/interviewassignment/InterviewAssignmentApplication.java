package com.vistar.interviewassignment;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vistar.interviewassignment.beans.Location;
import com.vistar.interviewassignment.beans.State;
import com.vistar.interviewassignment.entity.StateJson;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class InterviewAssignmentApplication {

	public static void main(String[] args) {
		SpringApplication.run(InterviewAssignmentApplication.class, args);
	}

	@Bean
	public List<State> statesWithBorders() throws Exception{
        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        ObjectMapper objectMapper = new ObjectMapper();
        InputStream is = classloader.getResourceAsStream("states.json");
        List<State> statesWithBorders = new ArrayList<>();
        List<StateJson> states = objectMapper.readValue(is, new TypeReference<List<StateJson>>(){});
        for(StateJson state : states) {
            List<Location> borders = new ArrayList<>();
            State stateWithBorders = new State();
            stateWithBorders.setState(state.getState());
            for(List  border : state.getBorder()) {
                Location location = new Location((Double) border.get(1), (Double) border.get(0));
                borders.add(location);
            }
            stateWithBorders.setBorder(borders);
            statesWithBorders.add(stateWithBorders);
        }
        return statesWithBorders;
	}
}
