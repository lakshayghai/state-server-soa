package com.vistar.interviewassignment.services;

import com.vistar.interviewassignment.beans.Location;
import com.vistar.interviewassignment.beans.State;
import com.vistar.interviewassignment.utils.LatLongUtilities;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class LatitudeLongitudeService {

    private static final Double INFINITE = new Double(180);

    private List<State> statesWithBorders;

    @Autowired
    public LatitudeLongitudeService(List<State> statesWithBorders) {
        this.statesWithBorders = statesWithBorders;
    }

    public State getStateByLatLong(Location location) {

        State state = new State("Not inside any state");

        Map<String, List<Location>> statesMap = statesWithBorders.stream().collect(
                Collectors.toMap(State::getState, State::getBorder)
        );
        statesMap.forEach((K,V) -> {
            if(isInsideBorders(V,location)){
                state.setState(K);
            }
        });

        return state;
    }

    private Boolean isInsideBorders(List<Location> borders, Location location)
    {
        int numberOfIntersect = 0;
        Location extreme = new Location(location.getLatitude(), INFINITE );
        int n = borders.size() - 1;
        for(int i=0 ; i< borders.size() - 1; i++) {
            int next = (i + 1) % n;
            if(LatLongUtilities.doIntersect(borders.get(i),borders.get(next),location,extreme)) {
                if(LatLongUtilities.orientation(borders.get(i),location,borders.get(next)) == 0) {
                    return LatLongUtilities.onBorder(borders.get(i),location,borders.get(next));
                }
               numberOfIntersect++;
            }
        }
        return numberOfIntersect % 2 == 1;
    }
}
