package com.vistar.interviewassignment.utils;

import com.vistar.interviewassignment.beans.Location;

public class LatLongUtilities {

    public static boolean onBorder(Location location1, Location location2, Location location3) {

        if (location2.getLongitude() <= Math.max(location1.getLongitude(), location3.getLongitude())
                && location2.getLongitude()>= Math.min(location1.getLongitude(), location3.getLongitude())
                && location2.getLatitude() <= Math.max(location1.getLatitude(), location3.getLatitude())
                && location2.getLatitude() >= Math.min(location1.getLatitude(), location3.getLatitude())) {
            return true;
        }
        return false;
    }

    public static int orientation(Location location1, Location location2, Location location3) {

        Double slope = (location2.getLatitude() - location1.getLatitude()) * (location3.getLongitude() - location2.getLongitude())
                - (location2.getLongitude() - location1.getLongitude()) * (location3.getLatitude() - location2.getLatitude());

        if (slope == 0) {
            return 0;
        }
        return (slope > 0) ? 1 : 2;
    }

    public static boolean doIntersect(Location location1, Location location2, Location location3, Location location4) {
        
        int orientation1 = orientation(location1, location2, location3);
        int orientation2 = orientation(location1, location2, location4);
        int orientation3 = orientation(location3, location4, location1);
        int orientation4 = orientation(location3, location4, location2);

        if (orientation1 != orientation2 && orientation3 != orientation4) {
            return true;
        }
        if (orientation1 == 0 && onBorder(location1, location2, location3)) {
            return true;
        }
        if (orientation2 == 0 && onBorder(location1, location4, location3)) {
            return true;
        }
        if (orientation3 == 0 && onBorder(location2, location1, location4)) {
            return true;
        }
        if (orientation4 == 0 && onBorder(location2, location3, location4)) {
            return true;
        }
        return false;
    }

}
