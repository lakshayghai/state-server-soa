package com.vistar.interviewassignment.controllers;

import com.vistar.interviewassignment.beans.Location;
import com.vistar.interviewassignment.beans.State;
import com.vistar.interviewassignment.services.LatitudeLongitudeService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/latitude-longitude")
public class LatitudeLongitudeController {

    @Autowired
    LatitudeLongitudeService latitudeLongitudeService;

    @ApiOperation(value = "Get the State Name based on the Latitude and Longitude that we input")
    @PostMapping(value = "/state")
    public State getStateByLatitudeLongitude(@Valid @RequestBody Location location) {
        return latitudeLongitudeService.getStateByLatLong(location);

    }
}
