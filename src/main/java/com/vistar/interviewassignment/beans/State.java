package com.vistar.interviewassignment.beans;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class State {

    private String state;
    private List<Location> border;

    public State() {
    }

    public State(String state) {
        this.state = state;
    }

    public State(String state, List<Location> border) {
        this.state = state;
        this.border = border;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public List<Location> getBorder() {
        return border;
    }

    public void setBorder(List<Location> border) {
        this.border = border;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        State state = (State) o;
        return Objects.equals(this.state, state.state) &&
                Objects.equals(border, state.border);
    }

    @Override
    public int hashCode() {
        return Objects.hash(state, border);
    }
}
