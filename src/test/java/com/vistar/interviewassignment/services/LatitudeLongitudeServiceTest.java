package com.vistar.interviewassignment.services;

import com.vistar.interviewassignment.beans.Location;
import com.vistar.interviewassignment.beans.State;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class LatitudeLongitudeServiceTest {


    @Test
    public void testGetStateByLatLong() {

        List<Location> bordersMaryland = new ArrayList<>();
        bordersMaryland.add(new Location(38.649665,-75.710712));
        bordersMaryland.add(new Location(39.720274,-79.480971));
        bordersMaryland.add(new Location(39.723866,-75.791094));
        bordersMaryland.add(new Location(38.649665,-75.710712));

        List<Location> bordersNewYork = new ArrayList<>();
        bordersNewYork.add(new Location(42.267327,-79.763235));
        bordersNewYork.add(new Location(45.006138,-73.344723));
        bordersNewYork.add(new Location(40.704002,-74.006183));
        bordersNewYork.add(new Location(42.267327,-79.763235));

        State state1 = new State("Maryland", bordersMaryland);
        State state2 = new State("New York",bordersNewYork);
        List<State> states = new ArrayList<>();
        states.add(state1);
        states.add(state2);

        Location locationToSearch1 = new Location(38.965453, -76.479212);
        LatitudeLongitudeService latitudeLongitudeService = new LatitudeLongitudeService(states);
        assert(latitudeLongitudeService.getStateByLatLong(locationToSearch1).equals(new State("Maryland")));

        Location locationToSearch2 = new Location(31.965453, -76.479212);
        assert(latitudeLongitudeService.getStateByLatLong(locationToSearch2).equals(new State("Not inside any state")));
    }
}
