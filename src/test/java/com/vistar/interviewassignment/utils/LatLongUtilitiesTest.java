package com.vistar.interviewassignment.utils;


import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import com.vistar.interviewassignment.beans.Location;
import org.junit.Test;

public class LatLongUtilitiesTest {

    @Test
    public void testOrientation() {
        Location location1 = new Location(40.513799, -77.036133);
        Location location2 = new Location(39.719623, -77.475793);
        Location location3 = new Location(39.721209, -80.524269);
        int orientation = LatLongUtilities.orientation(location1, location2, location3);
        assert(orientation != 0);

    }

    @Test
    public void testOnSegment() {

        Location location1 = new Location(40.513799, -77.036133);
        Location location2 = new Location(39.719623, -77.475793);
        Location location3 = new Location(39.721209, -80.524269);

        Boolean onSegment = LatLongUtilities.onBorder(location1,location2,location3);
        assertFalse(onSegment);
    }

    @Test
    public void testDoIntersect() {
        Location location1 = new Location(38.649665, -75.710712);
        Location location2 = new Location(39.723866, -75.791094);
        Location location3 = new Location(38.965453, -76.479212);
        Location location4 = new Location(38.965453, 180.00000);

        Boolean intersect =  LatLongUtilities.doIntersect(location1,location2,location3,location4);
        assertTrue(intersect);
    }

}
